﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsExampleApp
{
    /// <summary>
    /// Обобщенные типы (generics) позволяют указать конкретный тип, который будет использоваться. В отличие от объектов (позволяющих нестрого типизировать),
    /// им не требуется распаковка и упаковка, что экономит память.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class Account<T>
    {
        public T Id { get; set; }
        // Оператор default(T) присваивает ссылочным типам в качестве значения null, а типам значений - значение 0.
        T someField = default(T);
        public int Sum { get; set; }
    }
    /// <summary>
    /// В этом классе поле Id является объектом, а не полем обобщённого типа. Также есть проблема типонебезопасности, ибо,
    /// неизвестно какий именно объект представляет Id, и, например, при попытке получить число из Id в следующем фрагменте кода
    /// Account account2 = new Account { Sum = 4000 };
    /// account2.Id = "4356";
    /// int id2 = (int)account2.Id;
    /// возникнет исключение InvalidCastException.
    /// </summary>
    class AccountWithObject
    {
        public object Id { get; set; }
        public int Sum { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Вариант с обобщённым типом.
            Account<int> account1 = new Account<int> { Sum = 5000 };
            Account<string> account2 = new Account<string> { Sum = 4000 };
            account1.Id = 2;        // Упаковка не нужна.
            account2.Id = "4356";
            int id1 = account1.Id;  // Распаковка не нужна.
            string id2 = account2.Id;
            Console.WriteLine(id1);
            Console.WriteLine(id2);

            // Вариант с объектом.
            AccountWithObject accountObj1 = new AccountWithObject { Sum = 5000 };
            AccountWithObject accountObj2 = new AccountWithObject { Sum = 4000 };
            accountObj1.Id = 2;
            accountObj2.Id = "4356";
            int idObj1 = (int)account1.Id; // Упаковка нужна.
            string idObj2 = (string)account2.Id; // Распаковка нужна.
            Console.WriteLine(idObj1);
            Console.WriteLine(idObj2);

            // Использование обобщённого метода Swap<T> как с числовым типом, так и со строковым.
            int x = 7;
            int y = 25;
            Swap<int>(ref x, ref y);
            Console.WriteLine($"x={x}    y={y}");   // x=25   y=7

            string s1 = "hello";
            string s2 = "bye";
            Swap<string>(ref s1, ref s2);
            Console.WriteLine($"s1={s1}    s2={s2}"); // s1=bye   s2=hello

            Console.Read();
        }

        /// <summary>
        /// Пример обобщённого метода. Данный метод принимает параметры по ссылке и меняет их значения.
        /// </summary>
        /// <typeparam name="T">Обобщённый тип.</typeparam>
        /// <param name="x">Первый параметр.</param>
        /// <param name="y">Второй параметр.</param>
        public static void Swap<T>(ref T x, ref T y)
        {
            T temp = x;
            x = y;
            y = temp;
        }
    }
}
